package Collection;
import java.util.*;
	  public class Example12 {
	  public static void main(String[] args) {
	  ArrayList<String>al = new ArrayList<String>();
	  al.add("Red");
	  al.add("Green");
	  al.add("Orange");
	  al.add("White");
	  al.add("Black");
	  System.out.println("Original list: " +al);
	  List<String> sub_List = al.subList(0, 3);
	  System.out.println("List of first three elements: " + sub_List);
	 }
	}
