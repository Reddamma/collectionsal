package Collection;
//Write a Java program to empty an array list.

	import java.util.ArrayList;
	import java.util.Arrays;
	 
	public class Example17
	{
	    public static void main(String[] args) throws Exception
	    {
	        ArrayList<String> list = new ArrayList<>(Arrays.asList("a", "b", "c", "d", "e"));
	        System.out.println(list);
	        list.clear();
	        System.out.println(list);
	    }
	}
	

