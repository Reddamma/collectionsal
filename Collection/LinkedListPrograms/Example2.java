package Collection.LinkedListPrograms;
import java.util.LinkedList;
import java.util.Iterator;
  public class Example2 {
  public static void main(String[] args) {
    
     LinkedList<String>al= new LinkedList<String>();
     al.add("Red");
     al.add("Green");
     al.add("Black");
     al.add("Pink");
     al.add("orange");
   System.out.println("Original linked list:" +al);  
    Iterator it = al.descendingIterator();
     System.out.println("Elements in Reverse Order:");
     while (it.hasNext()) {
        System.out.println(it.next());
     }
  }
}