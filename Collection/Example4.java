package Collection;
// Write a Java program to retrieve an element (at a specified index) from a given array list.

	import java.util.*;
	  public class Example4 {
	  public static void main(String[] args) {
	 ArrayList<String> al = new ArrayList<String>();
	  al.add("Red");
	  al.add("Green");
	  al.add("Orange");
	  al.add("White");
	  al.add("Black");
	  System.out.println(al);
	  String element = al.get(0);
	  System.out.println("First element: "+element);
	  element =al.get(2);
	  System.out.println("Third element: "+element);
	 }
}
