package Collection;
//Write a Java program to remove the third element from a array list.
	import java.util.*;
	  public class Example6 {
	  public static void main(String[] args) {
	 ArrayList<String>al= new ArrayList<String>();
	  al.add("Red");
	  al.add("Green");
	  al.add("Orange");
	  al.add("White");
	  al.add("Black");
	  System.out.println(al);
	  al.remove(2);
	  System.out.println("After removing third element from the list"+al);
	 }
	}

