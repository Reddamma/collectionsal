package Collection;
//Write a Java program of swap two elements in an array list.

	import java.util.ArrayList;
	import java.util.Collections;
	  public class Example14 {
	  public static void main(String[] args) {
	   ArrayList<String> al= new ArrayList<String>();
	   al.add("Red");
	   al.add("Green");
	   al.add("Black");
	   al.add("White");
	   al.add("Pink");
	   System.out.println("Array list before Swap:");
	   for(String a:al){
	   System.out.println(a);
	   }
	   Collections.swap(al, 0, 2);
	   System.out.println("Array list after swap:");
	   for(String b:al){
	   System.out.println(b);
	         }
	     }
	}

