package Collection;
//Write a Java program to search an element in a array list.
import java.util.*;
	  public class Example7 {
	  public static void main(String[] args) {
	  List<String> al = new ArrayList<String>();
	  al.add("Red");
	  al.add("Green");
	  al.add("Orange");
	  al.add("White");
	  al.add("Black");
	  if ( al.contains("yellow")) {
	    System.out.println("Found the element");
	    } else {
	    System.out.println("There is no such element");
	    }
	 }
}
