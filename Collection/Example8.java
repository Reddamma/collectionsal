package Collection;
//Write a Java program to sort a given array list.

	import java.util.*;
	  public class Example8 {
	  public static void main(String[] args) {
	  ArrayList<String> al= new ArrayList<String>();
	  al.add("Red");
	  al.add("Green");
	  al.add("Orange");
	  al.add("White");
	  al.add("Black");
	  System.out.println("List before sort: "+al);
	  Collections.sort(al);
	  System.out.println("List after sort: "+al);    
	 }
	}

