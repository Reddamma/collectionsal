package Collection;

import java.util.ArrayList;

public class Example18 {
	
	    public static void main(String[] args)
	    {
	        ArrayList<String> list = new ArrayList<String>();
	         
	        System.out.println(list.size());
	         
	        list.add("A");
	         
	        System.out.println(list.size()); 
	         
	        list.clear();
	         
	        System.out.println(list.size() == 0); 
	    }
	}

